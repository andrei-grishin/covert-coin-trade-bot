# Coin Trade Bot

The bot is running in https://t.me/covert_coin_trade_bot

Required python3.9 or above

##### Install requirements
```
pip install -r requirements.txt
```

##### Run tg bot
In the module config.py set your telegram bot token
```
python create_tables.py - Create SQLite db file and tables
python run.py - Run bot
```

##### Run tests
```
pytest tests -vs
```
