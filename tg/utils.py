from contextlib import asynccontextmanager

from telegram import Message

from tg.responder import Responder


@asynccontextmanager
async def exception_handler(message: Message) -> None:
    try:
        yield
    except:
        await Responder.reply_error_unknown(message)
        raise
