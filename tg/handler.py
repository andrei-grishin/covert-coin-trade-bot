from telegram import Update, Message
from telegram.constants import ChatType
from telegram.ext import ContextTypes

from config import DB_NAME
from db.sqlite import create_cursor
from managers.exchange_coin import ExchangeCoinsManager
from managers.parser import ParserManager
from managers.appeal import AppealManager
from managers.report import ReportManager
from managers.user import UserManager
from repository.sqlite import SQLiteRepository
from services.coin_exchange.fake import FakeCoinsExchangeDealAPI
from services.coin_price.coin_gecko import CoinGeckoAPI
from tg.responder import Responder
from tg.utils import exception_handler


class Handler:

    @staticmethod
    async def handle_start_command(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
        user_tg_id: int = update.message.chat.id

        with create_cursor(DB_NAME) as cursor:
            await UserManager.greet_user(
                message=update.message, repository=SQLiteRepository(cursor), user_tg_id=user_tg_id
            )

    @staticmethod
    async def handle_help_command(update: Update, context: ContextTypes.DEFAULT_TYPE):
        await Responder.reply_help(update.message)

    @classmethod
    async def handle_message_with_exceptions(cls, update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
        message: Message = update.message
        async with exception_handler(message):
            await cls._handle_message(message)

    @staticmethod
    async def _handle_message(message: Message) -> None:
        message_type: str = message.chat.type
        if message_type != ChatType.PRIVATE:
            await Responder.reply_error_only_private_chat(message)
            return None

        user_tg_id: int = message.chat.id
        text: str = message.text
        processed_text: str = text.lower().strip()

        # Make coins exchange deal
        if ParserManager.is_exchange_coins_operation(text=processed_text):
            data = ParserManager.get_data_for_exchange_coins(text=processed_text)

            with create_cursor(DB_NAME) as cursor:
                await ExchangeCoinsManager.do_exchange_coins(
                    message=message,
                    user_tg_id=user_tg_id,
                    data=data,
                    repository=SQLiteRepository(cursor),
                    coin_deal_api=FakeCoinsExchangeDealAPI(),
                    coin_price_api=CoinGeckoAPI()
                )

        # Make report
        elif ParserManager.is_report_operation(text=processed_text):

            with create_cursor(DB_NAME) as cursor:
                await ReportManager.make_report(
                    repository=SQLiteRepository(cursor), message=message, user_tg_id=user_tg_id
                )

        # Save appeal
        else:

            with create_cursor(DB_NAME) as cursor:
                await AppealManager.save_user_appeal(
                    repository=SQLiteRepository(cursor), message=message, user_tg_id=user_tg_id, text=text
                )
