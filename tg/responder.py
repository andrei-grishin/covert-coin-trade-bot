from telegram import Message

from models.account import Account
from models.deal import Deal
from models.enum import CoinEnum


class Responder:

    @classmethod
    async def reply_report(
            cls, message: Message, accounts: tuple[Account, Account, Account], deals: list[Deal]
    ) -> None:
        report_lines = [
            'Your balances:',
            f'{accounts[0].coin.repr_value}  = {accounts[0].balance_repr}',
            f'{accounts[1].coin.repr_value}  = {accounts[1].balance_repr}',
            f'{accounts[2].coin.repr_value}  = {accounts[2].balance_repr}',
            '',
            'Your deals: (time is in UTC)'
        ]

        for deal in deals:
            report_lines.append(
                f'{deal.created_at}, +{deal.buy_amount_repr} {deal.buy_coin.repr_value}, '
                f'-{deal.sell_amount_repr} {deal.sell_coin.repr_value}, {deal.status.repr_value}'
            )
            report_lines.append('')

        await cls._reply_to_message(message=message, text='\n'.join(report_lines))

    @classmethod
    async def reply_help(cls, message: Message) -> None:
        await cls._reply_to_message(
            message=message,
            text='You can use the message in format like: <SELL/BUY> <VALUE> <FIRST_COIN>/<SECOND_COIN>\n'
            '<SELL/BUY> - Operation that you want SELL or BUY\n'
            '<VALUE> - Numerical value of coin that you want to buy or sell\n'
            '<FIRST_COIN> - The coin you want buy or sell\n'
            '<SECOND_COIN> - The coin you want to trade for\n'
            'Available coins: btc, eth, usdt'
            '\n'
            'Examples:\n'
            'buy 100 BTC/USDT - Buy 100 btc for usdt\n'
            'Sell 0.3 Eth/BTC - Sell 0.3 eth and get btc\n'
            '\n'
            'Furthermore you can get report of you balances and your deals, just send the message "report"\n'
            '\n'
            'If you have any appeals you can send any other message, with will save it, consider and answer you later'
        )

    @classmethod
    async def reply_error_the_same_coins(cls, message: Message) -> None:
        await cls._reply_to_message(message=message, text="Please use various coins")

    @classmethod
    async def reply_error_amount_zero(cls, message: Message) -> None:
        await cls._reply_to_message(message=message, text="Please use the value more than 0")

    @classmethod
    async def reply_error_unknown(cls, message: Message) -> None:
        await cls._reply_to_message(message=message, text="Sorry, there was an unknown server error.")

    @classmethod
    async def reply_appeal_accepted(cls, message: Message) -> None:
        await cls._reply_to_message(
            message=message,
            text="Your appeal has been accepted and will be considered soon.\n\n"
                 "If you didn't intend to do this, please use the command /help to see other options."
        )

    @classmethod
    async def reply_welcome_back(cls, message: Message) -> None:
        await cls._reply_to_message(message=message, text="Welcome back!")

    @classmethod
    async def reply_welcome(cls, message: Message) -> None:
        await cls._reply_to_message(message=message, text="Welcome! You are ready for trading!")

    @classmethod
    async def reply_deal_is_approved(
            cls,
            message: Message,
            buy_amount_repr: str,
            sell_amount_repr: str,
            buy_coin: CoinEnum,
            sell_coin: CoinEnum
    ) -> None:
        await cls._reply_to_message(
            message=message,
            text=f"Your deal is succeed!\nYou bought {buy_amount_repr} {buy_coin},\n"
                 f"you sold {sell_amount_repr} {sell_coin.repr_value}"
        )

    @classmethod
    async def reply_deal_is_declined(cls, message: Message) -> None:
        await cls._reply_to_message(message=message, text="Sorry, but your deal was declined")

    @classmethod
    async def reply_not_enough_balance(
            cls, message: Message, balance_repr: str, amount_repr: str, coin: CoinEnum
    ) -> None:
        await cls._reply_to_message(
            message=message, text=f"You don't have enough balance: {balance_repr} < {amount_repr} {coin.repr_value}"
        )

    @classmethod
    async def reply_error_only_private_chat(cls, message: Message) -> None:
        await cls._reply_to_message(message=message, text='Sorry, I can only work in a private chat')

    @staticmethod
    async def _reply_to_message(message: Message, text: str) -> None:
        await message.reply_text(text)
