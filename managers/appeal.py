from telegram import Message

from managers.user import UserManager
from models.user import User
from repository.interface import IRepository
from tg.responder import Responder


class AppealManager:
    @staticmethod
    async def save_user_appeal(repository: IRepository, message: Message, user_tg_id: int, text: str) -> None:
        """
        Save user appeal in storage
        :param repository: Repository object for access to data storage
        :param message: Message telegram object
        :param user_tg_id: User telegram id
        :param text: User appeal text
        :return:
        """
        user: User = UserManager.get_or_create_user(repository=repository, user_tg_id=user_tg_id)
        repository.add_appeal(user_id=user.id, text=text)

        await Responder.reply_appeal_accepted(message=message)
