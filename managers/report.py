from telegram import Message

from managers.account import AccountManager
from managers.user import UserManager
from models.user import User
from repository.interface import IRepository
from tg.responder import Responder


class ReportManager:
    @staticmethod
    async def make_report(repository: IRepository, message: Message, user_tg_id: int) -> None:
        """
        Send user the report of his balances and deals
        :param repository: Repository object for access to data storage
        :param message: Message telegram object
        :param user_tg_id: User id in tg
        :return: 3 user accounts, list of user deals
        """
        user: User = UserManager.get_or_create_user(repository=repository, user_tg_id=user_tg_id)
        accounts = AccountManager.get_accounts(repository=repository, user_id=user.id)
        deals = repository.get_deals_by_user_id(user_id=user.id)

        await Responder.reply_report(message=message, accounts=accounts, deals=deals)
