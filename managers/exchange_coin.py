from telegram import Message

from managers.account import AccountManager
from managers.user import UserManager
from models.account import Account
from models.deal import Deal
from models.enum import OperationTypeEnum, CoinEnum
from models.input_data import DataForExchangeCoins
from models.user import User
from repository.interface import IRepository
from services.coin_exchange.interface import ICoinsExchangeDealAPI
from services.coin_price.interface import ICoinExchangePriceAPI
from tg.responder import Responder
from utils.utils import get_multiplied_value


class ExchangeCoinsManager:
    @classmethod
    async def do_exchange_coins(
            cls,
            message: Message,
            user_tg_id: int,
            data: DataForExchangeCoins,

            repository: IRepository,
            coin_price_api: ICoinExchangePriceAPI,
            coin_deal_api: ICoinsExchangeDealAPI
    ) -> None:
        """
        Make a deal with specific coins if it's possible, save the result in storage
        :param message: Message telegram object
        :param user_tg_id: User telegram id
        :param data: object with data for make exchange coins
        :param repository: Repository object for db
        :param coin_price_api: Api for getting current coin prices
        :param coin_deal_api: Api for making deal with coins
        :return:
        """
        is_ok = await cls._is_input_data_ok(message=message, data=data)
        if not is_ok:
            return None

        user, from_account, to_account = cls._get_initial_data(
            repository=repository, user_tg_id=user_tg_id, buy_coin=data.buy_coin, sell_coin=data.sell_coin
        )

        deal: Deal = repository.add_deal(
            user_id=user.id,
            from_account_id=from_account.id,
            to_account_id=to_account.id,
            sell_amount=data.sell_amount_multiplied,
            buy_amount=data.buy_amount_multiplied,
        )

        # Find out provisional second value
        if data.operation_type == OperationTypeEnum.buy:
            exchange_value: float = coin_price_api.get_exchange_value(buy_coin=data.buy_coin, sell_coin=data.sell_coin)
            deal.sell_amount = get_multiplied_value(data.buy_amount * exchange_value)
        else:
            exchange_value: float = coin_price_api.get_exchange_value(buy_coin=data.sell_coin, sell_coin=data.buy_coin)
            deal.buy_amount = get_multiplied_value(data.sell_amount * exchange_value)

        # Send error if balance is not enough
        if from_account.balance < deal.sell_amount:
            await Responder.reply_not_enough_balance(
                message=message,
                amount_repr=deal.sell_amount_repr,
                balance_repr=from_account.balance_repr,
                coin=from_account.coin
            )
            deal.set_declined()
            repository.update_deal_amounts_and_status(deal=deal)
            return None

        # Make deal and accurate values of trade
        is_success, buy_amount, sell_amount = coin_deal_api.make_exchange_deal(
            coin_price_api=coin_price_api,
            buy_amount=data.buy_amount,
            sell_amount=data.sell_amount,
            current_balance=from_account.balance,
            buy_coin=data.buy_coin,
            sell_coin=data.sell_coin
        )
        deal.buy_amount = get_multiplied_value(buy_amount)
        deal.sell_amount = get_multiplied_value(sell_amount)

        # If deal is not successful send error
        if not is_success:
            deal.set_declined()
            repository.update_deal_amounts_and_status(deal=deal)
            await Responder.reply_deal_is_declined(message)
            return None

        # If deal is successful update all values in db and send answer
        deal.set_approved()

        from_account.decrease_balance(deal.sell_amount)
        to_account.increase_balance(deal.buy_amount)

        repository.update_account_balance(account=from_account)
        repository.update_account_balance(account=to_account)

        repository.update_deal_amounts_and_status(deal=deal)

        await Responder.reply_deal_is_approved(
            message=message,
            buy_amount_repr=deal.buy_amount_repr,
            sell_amount_repr=deal.sell_amount_repr,
            buy_coin=data.buy_coin,
            sell_coin=data.sell_coin
        )

    @staticmethod
    def _get_initial_data(
            repository: IRepository, user_tg_id: int, buy_coin: CoinEnum, sell_coin: CoinEnum
    ) -> tuple[User, Account, Account]:
        user = UserManager.get_or_create_user(repository=repository, user_tg_id=user_tg_id)
        from_account = AccountManager.get_or_create_account(repository=repository, user_id=user.id, coin=sell_coin)
        to_account = AccountManager.get_or_create_account(repository=repository, user_id=user.id, coin=buy_coin)

        return user, from_account, to_account

    @staticmethod
    async def _is_input_data_ok(message, data: DataForExchangeCoins) -> bool:
        # Forbidden the same coins
        if data.buy_coin == data.sell_coin:
            await Responder.reply_error_the_same_coins(message=message)
            return False

        # Forbidden amount less or equal zero
        if (
                (data.buy_amount is not None and data.buy_amount <= 0) or
                (data.sell_amount is not None and data.sell_amount <= 0)
        ):
            await Responder.reply_error_amount_zero(message=message)
            return False

        return True
