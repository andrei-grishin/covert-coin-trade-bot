from telegram import Message

from managers.account import AccountManager
from models.user import User
from repository.interface import IRepository
from tg.responder import Responder


class UserManager:

    @staticmethod
    async def greet_user(message: Message, user_tg_id: int, repository: IRepository) -> None:
        is_exist = repository.is_user_exist_by_tg_id(user_tg_id=user_tg_id)
        if is_exist:
            await Responder.reply_welcome_back(message=message)
            return None

        user: User = repository.add_user(user_tg_id=user_tg_id)
        AccountManager.add_accounts(repository=repository, user_id=user.id)

        await Responder.reply_welcome(message=message)

    @staticmethod
    def get_or_create_user(repository: IRepository, user_tg_id: int) -> User:
        is_exist: bool = repository.is_user_exist_by_tg_id(user_tg_id=user_tg_id)
        if is_exist:
            return repository.get_user_by_tg_id(user_tg_id=user_tg_id)
        return repository.add_user(user_tg_id=user_tg_id)
