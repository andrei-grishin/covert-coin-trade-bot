import re

from models.enum import OperationTypeEnum, CoinEnum
from models.input_data import DataForExchangeCoins

# buy, sell
operation_pattern = r'(buy|sell)'

# 100, 3.14, ...
number_value_pattern = r'\d+(\.\d+)?'

coin_names = ['btc', 'eth', 'usdt']

# (btc|eth|usdt): btc, eth, usdt
coin_pattern = rf"({'|'.join(coin_names)})"

# (btc|eth|usdt)/(btc|eth|usdt): btc/eth, usdt/btc, ...
coin_exchange_pattern = rf'{coin_pattern}/{coin_pattern}'

# ^(buy|sell) \d+(\.\d+)? (btc|eth|usdt)/(btc|eth|usdt): sell 100.0 btc/usdt, ...
total_exchange_coin_pattern = rf'^{operation_pattern} {number_value_pattern} {coin_exchange_pattern}$'

# report
report_pattern = r'^report$'


class ParserManager:
    @classmethod
    def is_exchange_coins_operation(cls, text: str) -> bool:
        return cls._match_pattern_text(pattern=total_exchange_coin_pattern, text=text)

    @classmethod
    def is_report_operation(cls, text: str) -> bool:
        return cls._match_pattern_text(pattern=report_pattern, text=text)

    @classmethod
    def get_data_for_exchange_coins(cls, text: str) -> DataForExchangeCoins:
        operation_type: str = cls._search_pattern_text(pattern=operation_pattern, text=text)
        operation_type_enum = OperationTypeEnum(operation_type)

        number_value: str = cls._search_pattern_text(pattern=number_value_pattern, text=text)
        number_value_float = float(number_value)

        coins_exchange = cls._search_pattern_text(pattern=coin_exchange_pattern, text=text)
        first_coin, second_coin = coins_exchange.split('/')

        return DataForExchangeCoins(
            operation_type=operation_type_enum,
            buy_coin=CoinEnum(first_coin) if operation_type_enum == OperationTypeEnum.buy else CoinEnum(second_coin),
            sell_coin=CoinEnum(first_coin) if operation_type_enum == OperationTypeEnum.sell else CoinEnum(second_coin),
            buy_amount=number_value_float if operation_type_enum == OperationTypeEnum.buy else None,
            sell_amount=number_value_float if operation_type_enum == OperationTypeEnum.sell else None,
        )

    @staticmethod
    def _match_pattern_text(pattern: str, text) -> bool:
        return bool(re.match(pattern, text))

    @staticmethod
    def _search_pattern_text(pattern: str, text) -> str:
        return re.search(pattern, text).group()
