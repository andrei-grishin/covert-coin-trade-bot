from config import INIT_BTC_BALANCE, INIT_ETH_BALANCE, INIT_USDT_BALANCE, MULTIPLIED_VALUE
from models.enum import CoinEnum
from models.account import Account
from repository.interface import IRepository


class AccountManager:
    @classmethod
    def get_or_create_account(cls, user_id: int, coin: CoinEnum, repository: IRepository) -> Account:
        """
        Get specific user account if it exists or create and return
        :param user_id: User id in application
        :param coin: The coin value of account
        :param repository: Repository object for access to data storage
        :return: Account in specific coin
        """
        is_exists: bool = repository.is_account_exists(user_id=user_id, coin=coin)
        if is_exists:
            return repository.get_account(user_id=user_id, coin=coin)
        return repository.add_account(user_id=user_id, coin=coin, init_balance=cls._get_initial_balance_by_coin(coin))

    @staticmethod
    def add_accounts(repository: IRepository, user_id: int) -> tuple[Account, Account, Account]:
        """
        Create and return all accounts for user by user's id
        :param repository: Repository object for access to data storage
        :param user_id: User id in application
        :return:
        """
        btc_account = repository.add_account(
            user_id=user_id, coin=CoinEnum.btc, init_balance=INIT_BTC_BALANCE * MULTIPLIED_VALUE
        )
        eth_account = repository.add_account(
            user_id=user_id, coin=CoinEnum.eth, init_balance=INIT_ETH_BALANCE * MULTIPLIED_VALUE
        )
        usdt_account = repository.add_account(
            user_id=user_id, coin=CoinEnum.usdt, init_balance=INIT_USDT_BALANCE * MULTIPLIED_VALUE
        )
        return btc_account, eth_account, usdt_account

    @staticmethod
    def get_accounts(repository: IRepository, user_id: int) -> tuple[Account, Account, Account]:
        """
        Get all user accounts by user's id in application
        :param repository: Repository object for access to data storage
        :param user_id: User id in application
        :return: BTC Account, ETH Account, USDT Account
        """
        btc_account: Account = repository.get_account(user_id=user_id, coin=CoinEnum.btc)
        eth_account: Account = repository.get_account(user_id=user_id, coin=CoinEnum.eth)
        usdt_account: Account = repository.get_account(user_id=user_id, coin=CoinEnum.usdt)
        return btc_account, eth_account, usdt_account

    @staticmethod
    def _get_initial_balance_by_coin(coin: CoinEnum) -> int:
        if coin == CoinEnum.btc:
            return INIT_BTC_BALANCE
        elif coin == CoinEnum.eth:
            return INIT_ETH_BALANCE
        elif coin == CoinEnum.usdt:
            return INIT_USDT_BALANCE
        else:
            raise ValueError
