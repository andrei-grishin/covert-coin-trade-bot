from sqlite3 import Cursor
from typing import Optional

from models.account import Account
from models.appeal import Appeal
from models.deal import Deal
from models.enum import CoinEnum, DealStatusEnum
from models.user import User
from repository.interface import IRepository
from utils.utils import get_utc_current_timestamp


class SQLiteRepository(IRepository):

    def __init__(self, cursor: Cursor):
        self._cursor = cursor

    def is_user_exist_by_tg_id(self, user_tg_id: int) -> bool:
        query = 'SELECT EXISTS (SELECT user.id FROM user WHERE user.tg_id = ?);'
        args = (user_tg_id,)

        result = self._cursor.execute(query, args)
        return bool(result.fetchone()[0])

    def add_user(self, user_tg_id: int) -> User:
        query = 'INSERT INTO user(created_at, tg_id) VALUES (?, ?) RETURNING id;'
        args = (get_utc_current_timestamp(), user_tg_id,)

        result = self._cursor.execute(query, args)
        id_ = result.fetchone()[0]
        return User(id=id_, tg_id=user_tg_id)

    def get_user_by_tg_id(self, user_tg_id: int) -> User:
        query = 'SELECT id, tg_id FROM user WHERE tg_id = ?;'
        args = (user_tg_id,)

        result = self._cursor.execute(query, args)
        row = result.fetchone()
        return User(id=row[0], tg_id=row[1])

    def add_account(self, user_id: int, coin: CoinEnum, init_balance: int) -> Account:
        query = 'INSERT INTO account(created_at, user_id, coin, balance) VALUES (?, ?, ?, ?) RETURNING id, balance;'
        args = (get_utc_current_timestamp(), user_id, coin, init_balance)

        result = self._cursor.execute(query, args)
        row = result.fetchone()
        return Account(id=row[0], user_id=user_id, coin=coin, balance=row[1])

    def update_account_balance(self, account: Account) -> None:
        query = 'UPDATE account SET balance = ? WHERE id = ?;'
        args = (account.balance, account.id)

        self._cursor.execute(query, args)

    def get_account(self, user_id: int, coin: CoinEnum) -> Account:
        query = 'SELECT id, balance FROM account WHERE user_id = ? AND coin = ?;'
        args = (user_id, coin)

        result = self._cursor.execute(query, args)
        row = result.fetchone()
        if not row:
            raise ValueError('Not Found')
        return Account(id=row[0], user_id=user_id, coin=coin, balance=row[1])

    def is_account_exists(self, user_id: int, coin: CoinEnum) -> bool:
        query = 'SELECT EXISTS (SELECT account.id FROM account WHERE user_id = ? AND coin = ?);'
        args = (user_id, coin)

        result = self._cursor.execute(query, args)
        return bool(result.fetchone()[0])

    def add_deal(
            self, user_id: int, from_account_id: int, to_account_id: int, sell_amount: Optional[int] = None,
            buy_amount: Optional[int] = None, status: DealStatusEnum = DealStatusEnum.requested
    ) -> Deal:
        query = """
            INSERT INTO deal(created_at, user_id, from_account_id, to_account_id, sell_amount, buy_amount, status) 
            VALUES (?, ?, ?, ?, ?, ?, ?) RETURNING id, created_at, status;
        """
        args = (get_utc_current_timestamp(), user_id, from_account_id, to_account_id, sell_amount, buy_amount, status)

        result = self._cursor.execute(query, args)
        row = result.fetchone()
        return Deal(
            id=row[0],
            created_at=row[1],
            user_id=user_id,
            from_account_id=from_account_id,
            to_account_id=to_account_id,
            status=DealStatusEnum(row[2]),
            sell_amount=sell_amount,
            buy_amount=buy_amount
        )

    def update_deal_amounts_and_status(self, deal: Deal) -> None:
        query = 'UPDATE deal SET sell_amount = ?, buy_amount = ?, status = ? WHERE id = ?'
        args = (deal.sell_amount, deal.buy_amount, deal.status, deal.id)

        self._cursor.execute(query, args)

    def get_deals_by_user_id(self, user_id: int) -> list[Deal]:
        query = """
            SELECT 
                deal.id, 
                deal.created_at,
                deal.from_account_id, 
                deal.to_account_id,
                deal.status,
                deal.sell_amount,
                deal.buy_amount,
                from_account.coin,
                to_account.coin
            FROM
                deal
                INNER JOIN account AS from_account ON deal.from_account_id = from_account.id
                INNER JOIN account AS to_account ON deal.to_account_id = to_account.id
            WHERE
                deal.user_id = ?
            ORDER BY deal.created_at DESC;
        """
        args = (user_id,)

        result = self._cursor.execute(query, args)
        rows = result.fetchall()
        return [Deal(
            id=row[0],
            created_at=row[1],
            user_id=user_id,
            from_account_id=row[2],
            to_account_id=row[3],
            status=DealStatusEnum(row[4]),
            sell_amount=row[5],
            buy_amount=row[6],
            sell_coin=CoinEnum(row[7]),
            buy_coin=CoinEnum(row[8])
        ) for row in rows]

    def add_appeal(self, user_id: int, text: str) -> Appeal:
        query = 'INSERT INTO appeal(created_at, user_id, text) VALUES (?, ?, ?) RETURNING id, created_at;'
        args = (get_utc_current_timestamp(), user_id, text)

        result = self._cursor.execute(query, args)
        row = result.fetchone()
        return Appeal(
            id=row[0],
            created_at=row[1],
            user_id=user_id,
            text=text
        )
