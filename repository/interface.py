from abc import ABC, abstractmethod
from typing import Optional

from models.account import Account
from models.appeal import Appeal
from models.deal import Deal
from models.enum import CoinEnum
from models.user import User


class IRepository(ABC):
    # =============================== User ===============================
    @abstractmethod
    def is_user_exist_by_tg_id(self, user_tg_id: int) -> bool:
        pass

    @abstractmethod
    def add_user(self, user_tg_id: int) -> User:
        pass

    @abstractmethod
    def get_user_by_tg_id(self, user_tg_id: int) -> User:
        pass
    # =======================================================================

    # =============================== Account ===============================
    @abstractmethod
    def add_account(self, user_id: int, coin: CoinEnum, init_balance: int) -> Account:
        pass

    @abstractmethod
    def update_account_balance(self, account: Account) -> None:
        pass

    @abstractmethod
    def get_account(self, user_id: int, coin: CoinEnum) -> Account:
        pass

    @abstractmethod
    def is_account_exists(self, user_id: int, coin: CoinEnum) -> bool:
        pass

    # =======================================================================

    # =============================== Deal ===============================
    @abstractmethod
    def add_deal(
            self,
            user_id: int,
            from_account_id: int,
            to_account_id: int,

            sell_amount: Optional[int] = None,
            buy_amount: Optional[int] = None
    ) -> Deal:
        pass

    @abstractmethod
    def update_deal_amounts_and_status(self, deal: Deal) -> None:
        pass

    @abstractmethod
    def get_deals_by_user_id(self, user_id: int) -> list[Deal]:
        pass
    # =======================================================================

    # =============================== Appeal ===============================
    @abstractmethod
    def add_appeal(self, user_id: int, text: str) -> Appeal:
        pass
    # =======================================================================
