from telegram.ext import Application, CommandHandler, MessageHandler, filters

from config import TOKEN
from tg.handler import Handler

if __name__ == '__main__':
    print('Starting bot...')
    app = Application.builder().token(TOKEN).build()

    app.add_handler(CommandHandler('start', Handler.handle_start_command))
    app.add_handler(CommandHandler('help', Handler.handle_help_command))

    app.add_handler(MessageHandler(filters.TEXT, Handler.handle_message_with_exceptions))

    print('Polling...')
    app.run_polling(poll_interval=2.0)
