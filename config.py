from typing import Final


# TG bot token
TOKEN: Final = 'YOUR TG BOT TOKEN'

# Value for multiple float to storage in db
MULTIPLIED_VALUE: Final = 1_000_000

# SQLite Database name
DB_NAME: Final = 'coin_trade.sqlite'


# Initial balances for new client
INIT_BTC_BALANCE: Final = 100
INIT_ETH_BALANCE: Final = 200
INIT_USDT_BALANCE: Final = 10_000
