import sqlite3
from contextlib import contextmanager
from sqlite3 import Cursor


@contextmanager
def create_cursor(db_name: str) -> Cursor:
    conn = sqlite3.connect(db_name)
    cursor = conn.cursor()
    try:
        yield cursor
        conn.commit()
    except:
        raise
    finally:
        conn.close()
