import sqlite3

from config import DB_NAME


def run():
    conn = sqlite3.connect(DB_NAME)
    cursor = conn.cursor()

    cursor.execute("""
        DROP TABLE IF EXISTS user;
    """)

    cursor.execute("""
        CREATE TABLE user(
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            created_at DATETIME NOT NULL,
            tg_id INTEGER UNIQUE NOT NULL
        );
    """)

    cursor.execute("""
        DROP TABLE IF EXISTS account;
    """)

    cursor.execute("""
        CREATE TABLE account(
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            created_at DATETIME NOT NULL,
            user_id INTEGER NOT NULL REFERENCES user(id),
            coin TEXT NOT NULL,
            balance BIGINT NOT NULL,
            CHECK ( coin IN ('btc', 'eth', 'usdt') ),
            CHECK ( balance >= 0 )
        );
    """)

    cursor.execute("""
        DROP TABLE IF EXISTS deal;
    """)

    cursor.execute("""
        CREATE TABLE deal(
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            created_at DATETIME NOT NULL,
            
            user_id INTEGER NOT NULL REFERENCES user(id),
            from_account_id INTEGER NOT NULL REFERENCES account(id),
            to_account_id INTEGER NOT NULL REFERENCES account(id),
            
            sell_amount BIGINT,
            buy_amount BIGINT,
            
            status TEXT NOT NULL,
            
            CHECK ( sell_amount IS NOT NULL OR buy_amount IS NOT NULL ),
            CHECK ( sell_amount > 0 AND buy_amount > 0 )
        );
    """)

    cursor.execute("""
        DROP TABLE IF EXISTS appeal;
    """)

    cursor.execute("""
        CREATE TABLE appeal(
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            created_at DATETIME NOT NULL,
            user_id INTEGER NOT NULL REFERENCES user(id),
            text VARCHAR(1000) NOT NULL
        );
    """)

    conn.commit()
    conn.close()


if __name__ == '__main__':
    run()
