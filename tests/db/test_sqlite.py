import os

from db.sqlite import create_cursor


def test_create_cursor():
    db_name = 'test_database.sqlite'

    with create_cursor(db_name) as cursor:
        result = cursor.execute('SELECT 2')
        value = result.fetchone()[0]
        assert value == 2

    os.remove(db_name)
