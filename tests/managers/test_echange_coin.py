import pytest

from config import MULTIPLIED_VALUE
from managers.exchange_coin import ExchangeCoinsManager
from models.account import Account
from models.enum import CoinEnum, OperationTypeEnum, DealStatusEnum
from models.input_data import DataForExchangeCoins
from models.user import User
from tests.mock import MockRepository


@pytest.mark.asyncio
async def test_do_exchange_coins(mock_message, mock_coin_price_api, mock_coin_deal_api):
    mock_repository = MockRepository()

    user_id = 1
    user_tg_id = 2
    btc_balance = 100 * MULTIPLIED_VALUE
    eth_balance = 200 * MULTIPLIED_VALUE

    user = User(id=user_id, tg_id=user_tg_id)
    btc_account = Account(id=1, user_id=user_id, balance=btc_balance, coin=CoinEnum.btc)
    eth_account = Account(id=2, user_id=user_id, balance=eth_balance, coin=CoinEnum.eth)

    mock_repository.users[user_tg_id] = user
    mock_repository.accounts[(user_id, CoinEnum.btc)] = btc_account
    mock_repository.accounts[(user_id, CoinEnum.btc)] = eth_account

    data = DataForExchangeCoins(
        buy_coin=CoinEnum.eth,
        sell_coin=CoinEnum.btc,
        buy_amount=None,
        sell_amount=100,
        operation_type=OperationTypeEnum.sell
    )

    mock_coin_price_api.get_exchange_value.return_value = 2.0
    mock_coin_deal_api.make_exchange_deal.return_value = (True, 200.0, 100.0)

    exchange_manager = ExchangeCoinsManager()
    await exchange_manager.do_exchange_coins(
        message=mock_message,
        user_tg_id=user_tg_id,
        data=data,
        repository=mock_repository,
        coin_price_api=mock_coin_price_api,
        coin_deal_api=mock_coin_deal_api
    )

    deal = mock_repository.deals[1]
    assert deal.status == DealStatusEnum.approved
