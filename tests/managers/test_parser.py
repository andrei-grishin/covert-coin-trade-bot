from managers.parser import ParserManager
from models.enum import OperationTypeEnum, CoinEnum


def test_is_exchange_coins_operation():
    assert ParserManager.is_exchange_coins_operation("buy 100.0 btc/eth")
    assert ParserManager.is_exchange_coins_operation("sell 3.14 usdt/btc")
    assert not ParserManager.is_exchange_coins_operation("report")
    assert not ParserManager.is_exchange_coins_operation("invalid text")


def test_is_report_operation():
    assert ParserManager.is_report_operation("report")
    assert not ParserManager.is_report_operation("buy 100.0 btc/eth")
    assert not ParserManager.is_report_operation("invalid text")


def test_get_data_for_exchange_coins():
    data = ParserManager.get_data_for_exchange_coins("buy 100.0 btc/eth")
    assert data.operation_type == OperationTypeEnum.buy
    assert data.buy_coin == CoinEnum.btc
    assert data.sell_coin == CoinEnum.eth
    assert data.buy_amount == 100.0
    assert data.sell_amount is None

    data = ParserManager.get_data_for_exchange_coins("sell 3.14 usdt/btc")
    assert data.operation_type == OperationTypeEnum.sell
    assert data.buy_coin == CoinEnum.btc
    assert data.sell_coin == CoinEnum.usdt
    assert data.buy_amount is None
    assert data.sell_amount == 3.14
