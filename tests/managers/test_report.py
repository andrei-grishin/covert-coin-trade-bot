import pytest

from managers.report import ReportManager
from models.account import Account
from models.enum import CoinEnum
from models.user import User
from tests.mock import MockRepository


@pytest.mark.asyncio
async def test_make_report(mock_message):
    mock_repository = MockRepository()

    user_id = 1
    user_tg_id = 2

    user = User(id=user_id, tg_id=user_tg_id)
    btc_account = Account(id=1, user_id=user_id, coin=CoinEnum.btc, balance=100)
    eth_account = Account(id=2, user_id=user_id, coin=CoinEnum.eth, balance=100)
    usdt_account = Account(id=3, user_id=user_id, coin=CoinEnum.usdt, balance=100)

    mock_repository.users[user_tg_id] = user
    mock_repository.accounts[(user_id, CoinEnum.btc)] = btc_account
    mock_repository.accounts[(user_id, CoinEnum.eth)] = eth_account
    mock_repository.accounts[(user_id, CoinEnum.usdt)] = usdt_account

    report_manager = ReportManager()
    await report_manager.make_report(repository=mock_repository, message=mock_message, user_tg_id=user_tg_id)
