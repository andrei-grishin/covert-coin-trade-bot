from unittest.mock import patch

import pytest

from managers.account import AccountManager
from models.enum import CoinEnum
from tests.mock import MockRepository


@pytest.fixture
def config_multiplied_value():
    multiplied_value = 1_000_000
    with patch('managers.account.MULTIPLIED_VALUE', multiplied_value):
        yield multiplied_value


@pytest.fixture
def config_init_btc_balance():
    init_btc_balance = 100
    with patch('managers.account.INIT_BTC_BALANCE', init_btc_balance):
        yield init_btc_balance


@pytest.fixture
def config_init_eth_balance():
    init_eth_balance = 200
    with patch('managers.account.INIT_ETH_BALANCE', init_eth_balance):
        yield init_eth_balance


@pytest.fixture
def config_init_usdt_balance():
    init_usdt_balance = 10_000
    with patch('managers.account.INIT_USDT_BALANCE', init_usdt_balance):
        yield init_usdt_balance


def test_get_or_create_account(config_init_btc_balance):
    repository = MockRepository()

    user_id = 1
    coin = CoinEnum.btc
    account = AccountManager.get_or_create_account(user_id, coin, repository)
    assert account.user_id == user_id
    assert account.coin == coin
    assert account.balance == config_init_btc_balance

    account2 = AccountManager.get_or_create_account(user_id, coin, repository)
    assert account.id == account2.id


def test_add_accounts(
        config_init_btc_balance, config_init_eth_balance, config_init_usdt_balance, config_multiplied_value
):
    repository = MockRepository()
    user_id = 2

    btc_account, eth_account, usdt_account = AccountManager.add_accounts(repository, user_id)
    assert btc_account.user_id == user_id
    assert btc_account.coin == CoinEnum.btc
    assert btc_account.balance == config_init_btc_balance * config_multiplied_value

    assert eth_account.user_id == user_id
    assert eth_account.coin == CoinEnum.eth
    assert eth_account.balance == config_init_eth_balance * config_multiplied_value

    assert usdt_account.user_id == user_id
    assert usdt_account.coin == CoinEnum.usdt
    assert usdt_account.balance == config_init_usdt_balance * config_multiplied_value


def test_get_accounts():
    repository = MockRepository()
    user_id = 3

    btc_account, eth_account, usdt_account = AccountManager.add_accounts(repository, user_id)

    btc_account2, eth_account2, usdt_account2 = AccountManager.get_accounts(repository, user_id)
    assert btc_account.id == btc_account2.id
    assert eth_account.id == eth_account2.id
    assert usdt_account.id == usdt_account2.id
