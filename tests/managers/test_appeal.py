import pytest

from managers.appeal import AppealManager
from tests.mock import MockRepository


@pytest.mark.asyncio
async def test_save_user_appeal(mock_message):
    mock_repository = MockRepository()

    user_tg_id = 123
    text = "Test appeal"

    appeal_manager = AppealManager()
    await appeal_manager.save_user_appeal(mock_repository, mock_message, user_tg_id, text)

    assert mock_repository.appeals[0].text == text
