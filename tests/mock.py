import datetime
from typing import Optional

from models.account import Account
from models.appeal import Appeal
from models.deal import Deal
from models.enum import CoinEnum, DealStatusEnum
from models.user import User
from repository.interface import IRepository


class MockRepository(IRepository):

    def __init__(self):
        self._counter = iter(range(1_000))

        self.users = {}
        self.accounts = {}
        self.deals = {}
        self.appeals = {}

    def next(self) -> int:
        return next(self._counter)

    def is_user_exist_by_tg_id(self, user_tg_id: int) -> bool:
        return user_tg_id in self.users

    def add_user(self, user_tg_id: int) -> User:
        user = User(id=self.next(), tg_id=user_tg_id)
        self.users[user_tg_id] = user
        return user

    def get_user_by_tg_id(self, user_tg_id: int) -> User:
        return self.users.get(user_tg_id)

    def add_account(self, user_id: int, coin: CoinEnum, init_balance: int) -> Account:
        account = Account(id=self.next(), user_id=user_id, coin=coin, balance=init_balance)
        self.accounts[(user_id, coin)] = account
        return account

    def update_account_balance(self, account: Account) -> None:
        self.accounts[(account.user_id, account.coin)] = account

    def get_account(self, user_id: int, coin: CoinEnum) -> Account:
        return self.accounts.get((user_id, coin))

    def is_account_exists(self, user_id: int, coin: CoinEnum) -> bool:
        return (user_id, coin) in self.accounts

    def add_deal(
            self,
            user_id: int,
            from_account_id: int,
            to_account_id: int,
            sell_amount: Optional[int] = None,
            buy_amount: Optional[int] = None
    ) -> Deal:
        deal = Deal(
            id=self.next(),
            created_at=datetime.datetime.now(),
            status=DealStatusEnum.requested,
            user_id=user_id,
            from_account_id=from_account_id,
            to_account_id=to_account_id,
            sell_amount=sell_amount,
            buy_amount=buy_amount
        )
        self.deals[deal.id] = deal
        return deal

    def update_deal_amounts_and_status(self, deal: Deal) -> None:
        self.deals[deal.id] = deal

    def get_deals_by_user_id(self, user_id: int) -> list[Deal]:
        return [deal for deal in self.deals.values() if deal.user_id == user_id]

    def add_appeal(self, user_id: int, text: str) -> Appeal:
        appeal = Appeal(id=self.next(), created_at=datetime.datetime.now(), user_id=user_id, text=text)
        self.appeals[len(self.appeals)] = appeal
        return appeal
