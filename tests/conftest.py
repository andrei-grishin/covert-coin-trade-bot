from unittest.mock import MagicMock

import pytest
from telegram import Message

from services.coin_exchange.interface import ICoinsExchangeDealAPI
from services.coin_price.interface import ICoinExchangePriceAPI


@pytest.fixture
def mock_message():
    return MagicMock(spec=Message)


@pytest.fixture
def mock_coin_price_api():
    return MagicMock(spec=ICoinExchangePriceAPI)


@pytest.fixture
def mock_coin_deal_api():
    return MagicMock(spec=ICoinsExchangeDealAPI)
