import datetime
from typing import Optional

from config import MULTIPLIED_VALUE


def get_multiplied_value(value: float) -> int:
    return int(value * MULTIPLIED_VALUE)


def get_divided_value(value: int) -> float:
    return value / MULTIPLIED_VALUE


def get_repr_value(value: Optional[float]) -> str:
    return f'{value:,}' if value is not None else 'NULL'


def get_utc_current_timestamp() -> datetime.datetime:
    dt = datetime.datetime.utcnow()
    return datetime.datetime(year=dt.year, month=dt.month, day=dt.day, hour=dt.hour, minute=dt.minute, second=dt.second)
