from dataclasses import dataclass

from models.enum import CoinEnum
from utils.utils import get_divided_value, get_repr_value


@dataclass
class Account:
    id: int
    user_id: int
    coin: CoinEnum
    balance: int

    @property
    def balance_divided(self) -> float:
        return get_divided_value(self.balance)

    @property
    def balance_repr(self) -> str:
        balance_divided = self.balance_divided
        return get_repr_value(balance_divided)

    def increase_balance(self, value: int) -> None:
        self._check_purpose_balance(value)
        self.balance += value

    def decrease_balance(self, value: int) -> None:
        self._check_purpose_balance(value * -1)
        self.balance -= value

    def _check_purpose_balance(self, value: int) -> None:
        if self.balance + value < 0:
            raise ValueError('Balance is below zero!')
