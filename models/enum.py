from enum import Enum


class CoinEnum(str, Enum):
    btc = 'btc'
    eth = 'eth'
    usdt = 'usdt'

    @property
    def repr_value(self) -> str:
        return self.value.upper()


class OperationTypeEnum(str, Enum):
    sell = 'sell'
    buy = 'buy'


class DealStatusEnum(str, Enum):
    requested = 'requested'
    approved = 'approved'
    declined = 'declined'

    @property
    def repr_value(self) -> str:
        return self.value.upper()
