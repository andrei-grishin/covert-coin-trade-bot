from dataclasses import dataclass


@dataclass
class User:
    id: int
    tg_id: int
