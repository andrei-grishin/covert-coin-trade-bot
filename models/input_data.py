from dataclasses import dataclass
from typing import Optional

from models.enum import OperationTypeEnum, CoinEnum
from utils.utils import get_multiplied_value


@dataclass
class DataForExchangeCoins:
    operation_type: OperationTypeEnum
    buy_coin: CoinEnum
    sell_coin: CoinEnum

    buy_amount: Optional[float] = None
    sell_amount: Optional[float] = None

    @property
    def buy_amount_multiplied(self) -> Optional[int]:
        if self.buy_amount:
            return get_multiplied_value(self.buy_amount)

    @property
    def sell_amount_multiplied(self) -> Optional[int]:
        if self.sell_amount:
            return get_multiplied_value(self.sell_amount)
