import datetime
from dataclasses import dataclass
from typing import Optional

from models.enum import DealStatusEnum, CoinEnum
from utils.utils import get_divided_value, get_repr_value

change_status_map = {
    DealStatusEnum.requested: [DealStatusEnum.approved, DealStatusEnum.declined],
    DealStatusEnum.approved: [],
    DealStatusEnum.declined: [],
}


@dataclass
class Deal:
    id: int
    created_at: datetime.datetime
    user_id: int
    from_account_id: int
    to_account_id: int
    status: DealStatusEnum

    sell_amount: Optional[int] = None
    buy_amount: Optional[int] = None
    sell_coin: Optional[CoinEnum] = None
    buy_coin: Optional[CoinEnum] = None

    @property
    def buy_amount_divided(self) -> Optional[float]:
        if not self.buy_amount:
            return None
        return get_divided_value(self.buy_amount)

    @property
    def buy_amount_repr(self) -> str:
        amount_divided = self.buy_amount_divided
        return get_repr_value(amount_divided)

    @property
    def sell_amount_divided(self) -> Optional[float]:
        if not self.sell_amount:
            return None
        return get_divided_value(self.sell_amount)

    @property
    def sell_amount_repr(self) -> str:
        amount_divided = self.sell_amount_divided
        return get_repr_value(amount_divided)

    def set_declined(self) -> None:
        self._set_status(new_status=DealStatusEnum.declined)

    def set_approved(self) -> None:
        self._set_status(new_status=DealStatusEnum.approved)

    def _set_status(self, new_status: DealStatusEnum) -> None:
        if new_status not in change_status_map[self.status]:
            raise ValueError(f'Wrong status transfer {self.status} -> {new_status}')

        self.status = new_status
