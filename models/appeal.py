import datetime
from dataclasses import dataclass


@dataclass
class Appeal:
    id: int
    created_at: datetime.datetime
    user_id: int
    text: str
