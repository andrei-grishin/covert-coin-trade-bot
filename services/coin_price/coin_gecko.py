import requests

from models.enum import CoinEnum
from services.coin_price.interface import ICoinExchangePriceAPI


class CoinGeckoAPI(ICoinExchangePriceAPI):
    @classmethod
    def get_exchange_value(cls, buy_coin: CoinEnum, sell_coin: CoinEnum) -> float:
        buy_coin_param = cls._get_buy_value_param_coin(buy_coin)
        sell_coin_param = cls._get_sell_value_param_coin(sell_coin)

        url = "https://api.coingecko.com/api/v3/simple/price"
        params = {
            "ids": buy_coin_param,
            "vs_currencies": sell_coin_param
        }

        response = requests.get(url, params=params)

        if response.status_code != 200:
            raise ValueError('Failed to get data from coin gecko')

        data = response.json()
        return data[buy_coin_param][sell_coin_param]

    @staticmethod
    def _get_buy_value_param_coin(coin: CoinEnum) -> str:
        if coin == CoinEnum.eth:
            return 'ethereum'
        elif coin == CoinEnum.btc:
            return 'bitcoin'
        elif coin == CoinEnum.usdt:
            return 'usd'

    @staticmethod
    def _get_sell_value_param_coin(coin: CoinEnum) -> str:
        if coin == CoinEnum.eth:
            return 'eth'
        elif coin == CoinEnum.btc:
            return 'btc'
        elif coin == CoinEnum.usdt:
            return 'usd'
