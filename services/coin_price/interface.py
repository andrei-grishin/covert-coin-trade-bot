from abc import ABC, abstractmethod

from models.enum import CoinEnum


class ICoinExchangePriceAPI(ABC):
    @staticmethod
    @abstractmethod
    def get_exchange_value(buy_coin: CoinEnum, sell_coin: CoinEnum) -> float:
        pass
