from abc import ABC, abstractmethod
from typing import Optional

from models.enum import CoinEnum
from services.coin_price.interface import ICoinExchangePriceAPI


class ICoinsExchangeDealAPI(ABC):

    @classmethod
    @abstractmethod
    def make_exchange_deal(
            cls,
            coin_price_api: ICoinExchangePriceAPI,
            buy_coin: CoinEnum,
            sell_coin: CoinEnum,
            current_balance: float,

            buy_amount: Optional[float] = None,
            sell_amount: Optional[float] = None
    ) -> tuple[bool, float, float]:
        """
        Make trade deal with third party service
        :param coin_price_api: Object for access to coin exchange price api
        :param buy_coin: The coin you can buy
        :param sell_coin: The coin you can sell
        :param current_balance: Current balance of user
        :param buy_amount: Value of coin you want to buy
        :param sell_amount: Value of coin you want to sell
        :return: Is successful deal or not, buy amount, sell amount
        """
        pass
