from typing import Optional

from models.enum import CoinEnum
from services.coin_exchange.interface import ICoinsExchangeDealAPI
from services.coin_price.interface import ICoinExchangePriceAPI


class FakeCoinsExchangeDealAPI(ICoinsExchangeDealAPI):

    @classmethod
    def make_exchange_deal(
            cls,
            coin_price_api: ICoinExchangePriceAPI,
            buy_coin: CoinEnum,
            sell_coin: CoinEnum,
            current_balance: float,

            buy_amount: Optional[float] = None,
            sell_amount: Optional[float] = None
    ) -> tuple[bool, float, float]:

        if not buy_amount and not sell_amount:
            raise ValueError('No one value is specified')
        if buy_amount and sell_amount:
            raise ValueError('Must be specified only one value')

        if buy_amount:
            exchange_value: float = coin_price_api.get_exchange_value(buy_coin=buy_coin, sell_coin=sell_coin)
            sell_amount = buy_amount * exchange_value
        else:
            exchange_value: float = coin_price_api.get_exchange_value(buy_coin=sell_coin, sell_coin=buy_coin)
            buy_amount = sell_amount * exchange_value

        if sell_amount > current_balance:
            is_successed = False
        else:
            is_successed = True

        return is_successed, buy_amount, sell_amount
